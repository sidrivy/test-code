<?php

namespace Sidrivy\TestCode\Tests;

use PHPUnit\Framework\TestCase;
use Sidrivy\TestCode\MathProcessor;
use Sidrivy\TestCode\Exceptions\InvalidElementFoundException;
use Sidrivy\TestCode\Exceptions\InvalidOperationException;
use Sidrivy\TestCode\Exceptions\UnexpectedElementFoundException;


class MathProcessorTest extends TestCase
{
    /**
     * @var MathProcessor
     */
    protected $mathProcessor;

    public function setUp()
    {
        $this->mathProcessor = new MathProcessor();
    }

    /**
     * Проверяем исключения, если переданы неверные символы.
     *
     * @dataProvider providerInvalidChars
     */
    public function testInvalidChars($query)
    {
        $this->expectException(InvalidElementFoundException::class);
        $this->mathProcessor->process($query);
    }

    public function providerInvalidChars()
    {
        return [
            ["2ю + 2"],
            ["2 - 1="],
            ["4 * 5\\"],
            ["6 /: 2"],
            ["2 +s 2 * 2"],
            ["56+0-d283"],
            ["1 +;1"],
            ["5 + <15"],
        ];
    }

    /**
     * Проверяем, что подсчет происходит правильный.
     *
     * @dataProvider providerProcess
     */
    public function testProcess($query, $solution)
    {
        $result = $this->mathProcessor->process($query);

        $this->assertEquals($solution, $result);
    }

    public function providerProcess()
    {
        return [
            ["2 + 2", 4],
            ["2 - 1", 1],
            ["4 * 5", 20],
            ["6 / 2", 3],
            ["2 + 2 * 2", 6],
            ["56+0-283", -227],
            ["1 +1- 9", -7],
            ["5 + 13 * 800 - 61 + 102 / 18 * 10 + 35 / 5 - 3 * 5 + 1 - 0 + 1 / 3 * 4 + 100", 10495],
        ];
    }

    /**
     * Проверяем, что пробелы не играют никакой роли.
     *
     * @dataProvider providerIgnoringSpaces
     */
    public function testIgnoringSpaces($query, $solution)
    {
        $result = $this->mathProcessor->process($query);

        $this->assertEquals($solution, $result);
    }

    public function providerIgnoringSpaces()
    {
        return [
            ["2 + 2", 4],
            ["2 + 2 * 2", 6],
            ["56+ 0 -283", -227],
            ["1 +1- 9", -7],
            ["5  +13 *8     - 61* 1 +  100/         10", 58],
        ];
    }

    /**
     * Проверяем исключения, если присутствует деление на ноль.
     *
     * @dataProvider providerDivisionByZero
     */
    public function testDivisionByZero($query)
    {
        $this->expectException(InvalidOperationException::class);
        $this->mathProcessor->process($query);
    }

    public function providerDivisionByZero()
    {
        return [
            ["1 / 0"],
            ["35 + 7 / 0"],
            ["0 / 0 + 10"],
            ["0 / 5 - 5 / 0"],
        ];
    }

    /**
     * Проверяем исключения, если элементы математической строки переданы в неверном порядке.
     *
     * @dataProvider providerIncorrectOrderOfElements
     */
    public function testIncorrectOrderOfElements($query)
    {
        $this->expectException(UnexpectedElementFoundException::class);
        $this->mathProcessor->process($query);
    }

    public function providerIncorrectOrderOfElements()
    {
        return [
            ["1 + "],
            [" / 0"],
            ["34 43"],
            ["2 +- 2"],
            ["1 / 1 + 4 - * 10"],
        ];
    }
}
