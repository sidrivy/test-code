<?php

namespace Sidrivy\TestCode;

use Sidrivy\TestCode\Exceptions\InvalidElementFoundException;
use Sidrivy\TestCode\Exceptions\InvalidOperationException;
use Sidrivy\TestCode\Exceptions\UnexpectedElementFoundException;

class MathProcessor
{
    const OPERATOR_PLUS     = '+';
    const OPERATOR_MINUS    = '-';
    const OPERATOR_MULTIPLY = '*';
    const OPERATOR_DIVIDE   = '/';

    /** @var array */
    protected $query = [];
    /** @var array */
    protected $queue = [];
    /** @var array */
    protected $operatorsPriorities = [
        self::OPERATOR_PLUS     => 1,
        self::OPERATOR_MINUS    => 1,
        self::OPERATOR_MULTIPLY => 2,
        self::OPERATOR_DIVIDE   => 2,
    ];

    /**
     * Решает переданую математическую строку.
     *
     * @param string $query
     *
     * @return int|float
     * @throws \Sidrivy\TestCode\Exceptions\InvalidElementFoundException
     * @throws \Sidrivy\TestCode\Exceptions\InvalidOperationException
     * @throws \Sidrivy\TestCode\Exceptions\UnexpectedElementFoundException
     */
    public function process($query)
    {
        $this->query = $query;

        $this->validateQuery();

        // return $this->solveQueryRaw();

        $this->parseQueryToQueue();

        return $this->solveQueue();
    }

    /**
     * @param string $operator
     * @param int|float $operand2
     * @param int|float $operand1
     *
     * @return int|float
     * @throws \Sidrivy\TestCode\Exceptions\InvalidOperationException
     * @throws \Sidrivy\TestCode\Exceptions\InvalidElementFoundException
     */
    protected function solveOperands($operator, $operand2, $operand1)
    {
        switch ($operator) {
            case self::OPERATOR_PLUS:
                return $operand1 + $operand2;
            case self::OPERATOR_MINUS:
                return $operand1 - $operand2;
            case self::OPERATOR_MULTIPLY:
                return $operand1 * $operand2;
            case self::OPERATOR_DIVIDE:
                if ($operand2 == 0.0) {
                    throw new InvalidOperationException("Division by zero");
                }
                return $operand1 / $operand2;
            default:
                throw new InvalidElementFoundException("Unknown operator '" . $operator . "'");
        }
    }

    /**
     * Проверяем наличие неразрешенных символов.
     *
     * @throws \Sidrivy\TestCode\Exceptions\InvalidElementFoundException
     */
    protected function validateQuery()
    {
        for ($i = 0; $i < strlen($this->query); $i++)
        {
            $char = $this->query[$i];
            if (
                !is_numeric($char)
                && !empty(trim($char))
                && !array_key_exists($char, $this->operatorsPriorities)
            ) {
                throw new InvalidElementFoundException("Invalid character found at position " . $i . " – '" . $char . "'");
            }
        }
    }

    /**
     * Простое решение математической строки через eval().
     * Предварительно нужно проверить "пользовательскую строку".
     * В общем случае небезопасно использовать eval().
     *
     * @return int|float
     */
    protected function solveQueryRaw()
    {
        return eval('return (' . $this->query . ');');
    }

    /**
     * Проводим анализ математической строки и преобразовываем в очередь постфиксной нотации.
     *
     * @return string
     * @throws \Sidrivy\TestCode\Exceptions\UnexpectedElementFoundException
     */
    protected function parseQueryToQueue()
    {
        $currentNumber = '';
        $isExpectingNumber = true;
        $isExpectingOperator = false;

        $stack = [];
        for ($i = 0; $i < strlen($this->query); $i++) {
            $char = $this->query[$i];

            if (is_numeric($char)) {
                if (!$isExpectingNumber) {
                    throw new UnexpectedElementFoundException("Unexpected number at position " . $i . " – '" . $char . "'");
                }
                $currentNumber .= $char;
            } elseif (strlen($currentNumber)) {
                $this->queue[] = $currentNumber;
                $currentNumber = '';
                $isExpectingNumber = false;
                $isExpectingOperator = true;
            }

            if (array_key_exists($char, $this->operatorsPriorities)) {
                if (!$isExpectingOperator) {
                    throw new UnexpectedElementFoundException("Unexpected operator at position " . $i . " – '" . $char . "'");
                }
                $wasOperatorSetToStack = false;
                while (!$wasOperatorSetToStack) {
                    // $lastStackOperator = end($stack);
                    // сначала использовал $lastStackOperator = end($stack);, но решил, что так будет более читаемо
                    // плюс при end() сдвигается указатель на элемент массива, может быть неявно в некоторых случах
                    $lastStackOperator = !empty($stack) ? $stack[count($stack) - 1] : '';
                    if (
                        $lastStackOperator
                        && $this->operatorsPriorities[$lastStackOperator] >= $this->operatorsPriorities[$char]
                    ) {
                        $this->queue[] = array_pop($stack);
                    } elseif (
                        !$lastStackOperator
                        || $lastStackOperator && $this->operatorsPriorities[$lastStackOperator] < $this->operatorsPriorities[$char]
                    ) {
                        $stack[] = $char;
                        $wasOperatorSetToStack = true;
                    }
                }
                $isExpectingOperator = false;
                $isExpectingNumber = true;
            }
        }
        if (strlen($currentNumber)) {
            $this->queue[] = $currentNumber;
            $isExpectingNumber = false;
        }

        if ($isExpectingNumber) {
            throw new UnexpectedElementFoundException("Unexpectedly ended without number");
        }

        while (!empty($stack)) {
            $this->queue[] = array_pop($stack);
        }
    }

    /**
     * Решаем выражение в постфиксной нотации из очереди.
     *
     * @return int|float
     * @throws \Sidrivy\TestCode\Exceptions\InvalidElementFoundException
     */
    protected function solveQueue()
    {
        $solutionStack = [];
        for ($i = 0; $i < count($this->queue); $i++) {
            $element = $this->queue[$i];

            if (is_numeric($element)) {
                $solutionStack[] = $element;
            } elseif (array_key_exists($element, $this->operatorsPriorities)) {
                $solutionStack[] = $this->solveOperands($element, array_pop($solutionStack), array_pop($solutionStack));
            } else {
                throw new InvalidElementFoundException("Invalid element in postfix queue at position " . $i . " – '" . $element . "'");
            }
        }
        $solution = array_pop($solutionStack);

        return $solution;
    }
}
